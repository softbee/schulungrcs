package ch.sbb.rcsd.test.systemtest.client;

import org.eclipse.core.runtime.IProgressMonitor;

import ch.sbb.rcsd.client.auth.IAuthenticationService;
import ch.sbb.rcsd.client.services.IServiceProvider;
import ch.sbb.rcsd.client.services.IServiceRegistry;
import ch.sbb.rcsd.transfer.benutzerverwaltung.Rolle;

interface IOpenSesameService {

}

public class OpenSesameService implements IOpenSesameService,
		IServiceProvider<IOpenSesameService> {

	public OpenSesameService() {
	}

	@Override
	public void destroy(final IProgressMonitor monitor) {
	}

	@Override
	public Class<?>[] getRequiredServices() {
		return new Class[] { IAuthenticationService.class, };
	}

	@Override
	public IOpenSesameService getService() {
		return this;
	}

	@Override
	public Class<IOpenSesameService> getServiceType() {
		return IOpenSesameService.class;
	}

	@Override
	public void init(final IServiceRegistry registry,
			final IProgressMonitor monitor) {
		final IAuthenticationService authService = registry
				.getService(IAuthenticationService.class);
		for (final Rolle rolle : authService.getAlleRollen()) {
			if ("Administrator".equals(rolle.getRollenName())) {
				authService.login("RCSProj", "RCSProj", rolle, null);
				// go
				break;
			}
		}
	}
}
