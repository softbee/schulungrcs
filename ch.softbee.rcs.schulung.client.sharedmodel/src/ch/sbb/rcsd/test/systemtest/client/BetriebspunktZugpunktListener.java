package ch.sbb.rcsd.test.systemtest.client;

import ch.sbb.rcsd.client.pmodel.IBetriebspunktZugpunkt;
import ch.sbb.rcsd.client.pmodel.IZuglauf;
import ch.sbb.rcsd.client.sharedmodel.IBetriebspunktZugpunktListener;

final class BetriebspunktZugpunktListener implements
		IBetriebspunktZugpunktListener {
	@Override
	public void removeBetriebspunktZugpunkt(IBetriebspunktZugpunkt zugpunkt) {

	}

	@Override
	public void addBetriebspunktZugpunkt(IBetriebspunktZugpunkt zugpunkt,
			IZuglauf zuglauf) {
		System.out.println(zugpunkt);
	}
}