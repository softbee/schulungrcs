package ch.sbb.rcsd.test.systemtest.client;

import static ch.sbb.rcsd.transfer.common.CommonTOFactoryHelper.newBetriebspunktZeitListe;
import static ch.sbb.rcsd.transfer.common.CommonTOFactoryHelper.newVorhaltebereich;

import java.util.Collection;
import java.util.Collections;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;

import ch.sbb.rcsd.client.environment.RCSDClientEnvironment;
import ch.sbb.rcsd.client.services.IServiceLifecycleManager;
import ch.sbb.rcsd.client.services.Services;
import ch.sbb.rcsd.client.sharedmodel.IBetriebspunktService;
import ch.sbb.rcsd.client.sharedmodel.IBewegungsdatenProvider;
import ch.sbb.rcsd.client.sharedmodel.IContinuousAccess;
import ch.sbb.rcsd.client.sharedmodel.IStammdatenProvider;
import ch.sbb.rcsd.common.date.RcsDateTime;
import ch.sbb.rcsd.transfer.common.BetriebspunktZeitListe;
import ch.sbb.rcsd.transfer.common.DunoEntityId;
import ch.sbb.rcsd.transfer.common.Vorhaltebereich;

public class MinimalApplication implements IApplication {

	private static final NullProgressMonitor NULL_PROGRESS_MONITOR = new NullProgressMonitor();

	private static final int MINUTE_IN_MS = 60*1000;

	private IServiceLifecycleManager sl;

	@Override
	public Object start(final IApplicationContext context) throws Exception {

		RCSDClientEnvironment
				.setActiveEnvironment("ch.sbb.rcsd.client.environment.bravo.bravo");

		sl = Services.getService(IServiceLifecycleManager.class);

		sl.initAllServices(NULL_PROGRESS_MONITOR);
		Services.getService(IStammdatenProvider.class).waitForStammdaten(
				NULL_PROGRESS_MONITOR);

		IContinuousAccess continuousAccess = Services.getService(
				IBewegungsdatenProvider.class).continuousAccess("Test");

		continuousAccess
				.setBetriebspunktZugpunktListener(new BetriebspunktZugpunktListener());

		BetriebspunktZeitListe interesse = createInteressengebiet();
		continuousAccess.setInteressengebiet(interesse);

		Thread.sleep(3 * MINUTE_IN_MS);

		return Status.OK_STATUS;
	}

	@Override
	public void stop() {
		sl.destroyAllServices(NULL_PROGRESS_MONITOR);
	}

	private BetriebspunktZeitListe createInteressengebiet() {
		Collection<DunoEntityId> c = Collections.singleton(Services
				.getService(IBetriebspunktService.class)
				.getBetriebspunktByKuerzel("BN").getBpId());
		RcsDateTime von = RcsDateTime.currentDateTime();
		RcsDateTime bis = new RcsDateTime(von.getMillis() + 15
				* MINUTE_IN_MS);
		Vorhaltebereich bereich = newVorhaltebereich(von, bis);
		BetriebspunktZeitListe interesse = newBetriebspunktZeitListe(c, bereich);
		return interesse;
	}

}
