package ch.softbee.rcs.schulung.graphviewer;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ITreeSelection;

import ch.sbb.rcsd.client.graphviewer.IDragModifier;
import ch.sbb.rcsd.client.graphviewer.IDropModifier;
import ch.sbb.rcsd.client.graphviewer.IGraphEditor;
import ch.sbb.rcsd.client.graphviewer.IRenderContext;
import ch.sbb.rcsd.client.ui.theme.ThemeCursors;

public class GraphViewerEditor implements IGraphEditor {

	@Override
	public String getCursor(Object element, Object detail) {
		if (element instanceof Logo) {
			return ThemeCursors.SYSTEM_HAND;
		} else {
			return null;
		}
	}

	@Override
	public IDragModifier beginDrag(ITreeSelection selection,
			final Object element, Object detail, int stateMask) {
		return new IDragModifier() {

			@Override
			public void renderDragShadow(IRenderContext rc, long x, long y,
					long dx, long dy) {
				rc.push(((Logo) element).x + dx, ((Logo) element).y + dy);
				rc.drawImage("ch.softbee.rcs.schulung.image1");

			}

			@Override
			public void modify(long x, long y, long dx, long dy) {
				((Logo) element).x += dx;
				((Logo) element).y += dy;
			}

			@Override
			public String getCursor(long x, long y, long dx, long dy) {
				return ThemeCursors.SYSTEM_SIZEALL;
			}
		};
	}

	@Override
	public IDropModifier beginDrop(ISelection selection) {
		// TODO Auto-generated method stub
		return null;
	}

}
