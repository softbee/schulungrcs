package ch.softbee.rcs.schulung.graphviewer;

import ch.sbb.rcsd.client.graphviewer.IToolTipProvider;

final class ToolTipProvider implements
		IToolTipProvider {
	@Override
	public String getToolTip(Object element, Object detail) {
		if(element instanceof Logo)
			return "Logo:"+((Logo)element).x + ":"+((Logo)element).y;
		return element.toString();
	}
}