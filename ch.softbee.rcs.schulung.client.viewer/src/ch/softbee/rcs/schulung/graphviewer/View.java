package ch.softbee.rcs.schulung.graphviewer;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;

import ch.sbb.rcsd.client.graphviewer.GraphViewer;
import ch.sbb.rcsd.client.graphviewer.OpenMagnifierHandler;
import ch.sbb.rcsd.client.graphviewer.WorldRectangle;

public class View extends ViewPart {
	public static final String ID = "ch.softbee.rcs.schulung.view";

	private GraphViewer viewer;

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 */
	public void createPartControl(Composite parent) {
		viewer = new GraphViewer(parent);
		
		
		GridDataFactory.fillDefaults().applyTo(viewer.getControl());
		viewer.setContentProvider(new ArrayContentProvider());
		viewer.setShapeProvider(new ShapeProvider());
		viewer.setViewPort(new WorldRectangle(0, 0, 100, 100));
		viewer.setInput(new Object[] { "One", "Two", "Three", new Logo() });
		
		
		viewer.setToolTipProvider(new ToolTipProvider());

		viewer.setGraphEditor(new GraphViewerEditor());
		
		OpenMagnifierHandler.install(viewer, this.getSite());
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		viewer.getControl().setFocus();
	}
}