package ch.softbee.rcs.schulung.graphviewer;

import org.eclipse.jface.viewers.BaseLabelProvider;
import org.eclipse.swt.SWT;

import ch.sbb.rcsd.client.graphviewer.IRenderContext;
import ch.sbb.rcsd.client.graphviewer.ISelectionStatus;
import ch.sbb.rcsd.client.graphviewer.IShapeProvider;

public class ShapeProvider extends BaseLabelProvider implements IShapeProvider {

	@Override
	public void render(Object element, IRenderContext rc,
			ISelectionStatus status) {
		if (element instanceof String) {
			if ("One".equals(element)) {
				
				rc.push(50, 10, -10,0);
				rc.push(50, 10, +10,0);
				
				rc.drawLine();
				
				rc.push(50, 10);
			} else if ("Two".equals(element)) {
				rc.push(50, 10, 0, rc.getTextSize((String) element).y);
			} else {
				rc.push(50, 10, 0, 2 * rc.getTextSize((String) element).y);
			}
			rc.pushHandle(null);

			if (status.isSelected()) {
				rc.setFont("ch.softbee.rcs.schulung.font.bold");
			} else {
				rc.setFont("ch.softbee.rcs.schulung.font.regular");
			}

			rc.drawText((String) element, SWT.CENTER, SWT.TOP);
		}

		if (element instanceof Logo) {
			rc.push(((Logo)element).x, ((Logo)element).y);
			rc.pushHandle(null);
			rc.drawImage("ch.softbee.rcs.schulung.image1");
		}

	}

	@Override
	public int getLayer(Object element, ISelectionStatus status) {
		return 0;
	}

}
